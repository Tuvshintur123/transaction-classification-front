import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue';
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueRouter from 'vue-router';
import FuckBudka from '@/components/FuckBudka.vue';
import Transactions from '@/components/Transactions.vue';
import Prediction from '@/components/Prediction.vue';


const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/', component: FuckBudka
    },
    {
      path: '/transactions', component: Transactions
    },
    {
      path: '/prediction', component: Prediction
    }
  ]
});

Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(BootstrapVue);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
